//Fichier suite-fibonacci.js
//Affichage de la suite de fibonacci avec le premier terme qui vaut 0 et le deuxième qui vaut 1.
//Pottier Sarah le 07/04/2021

//variables
let n1 = 0 ;            //premier nombre
let n2 = 1 ;            //deuxième nombre   
let res  = 0 ;          //resultat de la somme des termes
let chaine = "" + n1 + " " + n2 ;       //variable qui sert a l'affichage 

//pour i allant de 2 à 17 
for (let i = 2; i<17; i++){             //i étant le nombre de valeurs de la suite qui sera affiché
    res = n1 + n2 ;                     //resultat de la somme des deux derniers termes
    n1 = n2;                            //on décale la deuxième valeur a la première valeur 
    n2 = res ;                          //on met le resultat dans la deuxieme valeur 
    chaine = chaine + " " + res ;       //on affiche le resultat a chaque tour
}

//Affichage de la suite dans la console 
console.log("Le suite de Fibonacci" + chaine);
