//Fichier factorielle.js
//Affichage de la factorielle d'un nombre
//Pottier Sarah le 07/04/2021

//variables
let N = 10 ;                    //valeur du nombre 
let resFactorielle = 1 ;        //Resultat de la factorielle    
let chaine = "" + resFactorielle ;  //chaine pour l'affichage du calcul 

//pour i allant de 2 à la valeur de N
for(let i = 2; i <= N; i++){                        //i est l'intervalle de valeurs dont on fait la multiplication 
    resFactorielle = resFactorielle * i ;           //on fait le calcul de la factorielle pour chaque valeur de i
    chaine = chaine + "x"+ i;                       //on ajoute les valeurs de i pour montrer le calcul dans la chaine 
}

//Affichage dans la console
console.log("Calcul de factorielle");
console.log(N + "!= " + chaine + " = "+ resFactorielle);