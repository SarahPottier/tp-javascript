## Activité 2 Suite de Syracuse 

## Explication
  >En mathématiques, on appelle suite de Syracuse une suite d'entiers naturels définie de la manière suivante :

  >On part d'un nombre entier N plus grand que zéro. Si N est pair on le divise par 2, sinon on le multiplie par 3 et on ajoute 1. On répète cette opération tant que la dernière valeur calculée n'est pas arrivée à 1.

## Image du test du programme dans la console(v.1)
  ![Image du test de la console](images/Test_programme.png)


## Image du test du programme dans la console (v.2)
![Image du test de la console](images/Test_avec_ajout.png)