//Fichier syracuse.js
//Affichage de la suite de syracuse avec une valeur de départ choisie 
//Pottier Sarah le 07/04/2021

//Variables
let N = 14 ;                        //valeur de départ choisie 
let valeurs = "" +N;                //affichage pour la console
let compteur = 0;                   //compteur du nombre de tour parcourue pour arriver à 1
let maxValeur = N;                  //valeur maximale dans cette chaine

console.log("Suite Syracuse pour "+ N + ": ");      //Affichage 

//si Le nombre choisi est inférieur à 1
if(N< 1){
    console.log("Erreur N est inférieur à 0 ! ");                   //on ne peut pas faire la suite donc on affiche un message d'erreur
}
//sinon
else{
    //Tant que la valeur choisi est supérieur à 1
    while (N>1){
        //on cherche le maximum dans la chaine
        if (N > maxValeur){         //si le nombre est supérieur au maximum 
            maxValeur = N;          //on met la valeur de N dans maximum
        }
        if (N%2 ==0){               //si le nombre est pair
            N= N/2;                 //on le divise par 2
        }
        else{                       //sinon
            N = (N*3)+1;            //on le multiplie par 3 puis on lui ajoute 1
        }
        valeurs = valeurs + "-" + N;        //chaine pour l'affichage des valeurs
        compteur=compteur+1 ;               //on augmente le compteur 
    }
}


//Affichage dans la console 
console.log(valeurs);
console.log("Le temps de vol est : " + compteur );
console.log("L'altitude maximale est : " + maxValeur );