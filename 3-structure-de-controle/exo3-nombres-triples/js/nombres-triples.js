//Fichier nombres-triples.js
//Affichage d'une suite de 12 nombres après la valeur de
//départ dont chaque terme soit égal au triple du terme précédent
//Pottier Sarah le 07/04/2021


//Variable
let N = 2 ;             //valeur de départ 
let chaine = ""+N;      //variable qui sert à l'affichage des valeurs 
let res = N ;           //resultat de la multiplication 


//pour i allant de 0 à 11 
for (let i = 0; i < 12 ; i++){            //i étant de le nombre de valeurs qu'il y aura 
    res = res*3;                            //on fait la multiplication
    chaine = chaine + " " + res;            //on ajoute le résultat dans la chaine pour l'affichage 
    
}

//Affichage dans la console 
console.log("Valeur de départ :" + N);
console.log("Valeurs de la suite : "+ chaine );
