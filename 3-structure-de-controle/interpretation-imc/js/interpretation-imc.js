//Fichier interpretation-imc.js
//Affichage de l'imc et de son interprétation 
//Pottier Sarah le 07/04/2021

//Variables 
let taille = 175;                           //variable taille
let poids = 100;                            //variable poids
let interpretation;                         //variable interpretation

let IMC = (poids / ((taille/100)*(taille/100))) ;               //Calcul de l'imc

//On teste chaque valeur de l'imc pour lui donner une interprétation
if (IMC < 16.5){
    interpretation = "Dénutrition" ;
}
else if (IMC < 18.5){
    interpretation = "Maigreur" ;
}
else if (IMC < 25){
    interpretation = "Corpulence normale" ;
}
else if (IMC < 30){
    interpretation = "Surpoids" ;
}
else if (IMC < 35){
    interpretation = "Obésité modérée" ;
}
else if (IMC < 40){
    interpretation = "Obésité sévère " ;
}
else {
    interpretation = "Obésité morbide" ;
}

//Affichage dans la console 
console.log("Calcul de l'IMC :");
console.log("taille :" + taille + "cm");
console.log("poids :" + poids + "kg");
console.log("IMC :" + IMC.toFixed(1));
console.log("interpretation de l'IMC :" + interpretation);