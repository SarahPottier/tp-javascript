# Activité 1 Calculer IMC et interprétation

## Explication
> Calcul de l'imc d'une personne 
    > Il y a quatre variables : poids, taille, IMC et l'interprétation.
    Le poids et la taille sont initalisés avec certaines valeurs et pour le calcul de l'imc en divisant le poids (en kg) par la taille (en m) au carré.
    On teste pour pouvoir afficher la bonne interpretation de l'imc
    > Affichage des quatre variables.

## Image du test du programme dans la console

  ![Image du test de la console](images/test_interpretation.png)