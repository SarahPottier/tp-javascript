//Fichier conversion_euro_dollar.js
//Affichage de la conversion des euros en dollars jusqu'à 16384 euros (2^14)
//Pottier Sarah le 07/04/2021

//Variables
let euros = 1;                  //euros
let dollars = 1.65;             //dollars

//tant que euros est inférieur ou égal à 16384
while (euros <= 16384){
    dollars = 1.65*euros;           //on fait le calcul de la conversion
    console.log(euros + " euro(s) = "+ dollars.toFixed(2) + " dollar(s)");      //affichage du résultat de la conversion 
    euros = euros *2 ;              //on multiplie euros par 2 pour pouvoir arriver jusqu'à 16384 euros
    //si on veut la conversion pour chaque euros on mets :
    //euros = euros + 1
}
