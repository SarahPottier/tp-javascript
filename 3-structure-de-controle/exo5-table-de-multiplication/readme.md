# Exercice 5 :Table de multiplication

## Explication
> Affichage  les 20 premiers termes de la table de multiplication par 7, 
en signalant au passage (à l'aide d'une astérisque) ceux qui sont multiples de 3.

## Affichage du test du programme dans la console

  ![Image du test de la console](images/test_console.png)