//Fichier table-multiplication.js
//Affichage  les 20 premiers termes de la table de multiplication par 7, 
//en signalant au passage (à l'aide d'une astérisque) ceux qui sont multiples de 3.
//Pottier Sarah le 07/04/2021

let N = 7 ;             //valeur du nombre dont on veut la table, ici 7
let res ;               //résultat de la multiplication 
let chaine = "" ;    //sert à l'affichage de la chaine de caractère

//on pour x allant de 1 à 21
for(let x = 1; x <= 21; x++){       //x étant une variable qui varie de 1 à 21 
    res = N * x;                    //résultat de la multiplication
    chaine = chaine + " " + res ;   // ajoute du résultat dans la chaine de caractère
    //on teste les multiples de 3
    if ((res % 3) == 0){ //si le résultat est multiple de 3
        chaine = chaine + "*" ;     //on rajoute une astérisque à coté 
    }
}


//Affichage de la chaine de caractère dans la console
console.log(chaine);