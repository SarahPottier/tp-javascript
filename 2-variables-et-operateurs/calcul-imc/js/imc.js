//Fichier imc.js
//Affichage du poids, de la taille et de l'imc dans une console
//Pottier Sarah le 07/04/2021

let taille = 169;                               //variable taille
let poids = 59;                                 //variable poids
let IMC = (poids / ((taille/100)*(taille/100))) ;           //calcul de l'imc   


//Affichage
console.log("Calcul de l'IMC :");
console.log("taille :" + taille + "cm");
console.log("poids :" + poids + "kg");
console.log("IMC :" + IMC.toFixed(1));