# Exercice 1 : calculer IMC

## Explication
> Calcul de l'imc d'une personne 
    > Il y a trois variables : poids, taille et IMC.
    Le poids et la taille sont initalisés avec certaines valeurs et pour le calcul de l'imc en divisant le poids (en kg) par la taille (en m) au carré.
    > Affichage des trois variables

## Affichage du test du programme dans la console

  ![Image du test de la console](images/test_calcul_imc.png)