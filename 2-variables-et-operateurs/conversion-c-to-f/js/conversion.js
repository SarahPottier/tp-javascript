//Fichier conversion.js
//Affichage de la conversion d'une certaine valeur en °C en °F 
//Pottier Sarah le 07/04/2021

//variables
let temp_C = 22.6;                  //variable temperature Celcius
let temp_F;                         //variable temperature Fahrenheit

temp_F = temp_C * 1.8 + 32 ;        //calcul de la conversion. On passe de Celcius(°C) à Fahrenheit (°F)

//Affichage dans la console
console.log("Conversion Celcius(°C)/Fahrenheit (°F)");
console.log("Une température de "+ temp_C + "°C correspond à une température de "+ temp_F.toFixed(1) + "°F");

