# Activité Calculer surface

## Explication
> Calcul de la surface d'une pièce 
    > Il y a trois variables : longueur, largeur et surface.
    La longueur et la largeur sont initalisées avec certaines valeurs et pour le calcul de la surface on multiplie la longueur et la largeur.
     Affichage des trois variables

## Affichage du test du programme dans la console

  ![Image du test de la console](images/test_console.png)