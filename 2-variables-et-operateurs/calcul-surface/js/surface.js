//Fichier surface.js
//Affichage de la surface d'une pièce
//Pottier Sarah le 07/04/2021

//Variables
let longueur = 2;                           //variable longueur
let largeur = 3;                            //variable largeur 
let surface = longueur * largeur ;          //résultat du calcul de la surface

//Affichage dans la console
console.log("longueur de la pièce : " + longueur + "m");
console.log("largeur de la pièce : " + largeur + "m");
console.log("surface de la pièce : " + surface + "m²");
