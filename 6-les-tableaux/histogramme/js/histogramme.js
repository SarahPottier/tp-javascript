//Fichier "histogramme.js"
//Calcul et affichage d'un histogramme
//Pottier Sarah le 18/05/2021

//création du tableau de notes
let tabNotes = [19,12,14,5,12,8,3,12,8,4,19,16,14,8,12,14,12,11,12,4,11,13];
//affichage des notes 
console.log(tabNotes);

calculerRepartition = function (prmTabNotes) {
    //création d'un tableau multidimentionnel [notes, effectif]
    let repartition = [[0,0],[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0],[13,0],[14,0],[15,0],[16,0],[17,0],[18,0],[19,0],[20,0]];       // Création du tableau avec les notes possibles.
    for(let notes of prmTabNotes){ 
        repartition[notes][1]++;      // Ajout du compteur de notes identiques dans le tableau
    }
    return repartition;     // Retour de la répartition des notes.
}


TracerHistogramme = function(prmTabRepart){        // Fonction pour histogramme.
    let nbNotes;      // Déclaration du nombre de notes.
    for(nbNotes of prmTabRepart){
        console.log(nbNotes[0] + "\t" + ("*").repeat(nbNotes[1]));      // Affichage pour l'histogramme des étoiles en fonction du nombre de notes
    }
}

//Appel de la fonction "CalculerRepartition"
let repartition = calculerRepartition(tabNotes);      
// Affichage de la répartition.
console.log(repartition);    

 // Appel de la fonction TtracerHistogramme".
TracerHistogramme(repartition);      

