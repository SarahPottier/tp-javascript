//Fichier "moyenne.js"
//déclarer un tableau, calculer la somme et la moyenne 
//Affichage des valeurs du tableau, de la somme et de la moyenne
//Affichage en mode console
//Pottier Sarah le 04/05/2021


let listeValeurs = new Array(10,25,41,5,9,11);              //création d'un tableau avec des valeurs 
let somme = 0;                                              //variable somme
let moyenne = 0;                                            //variable moyenne
let compteur = 0;                                           //variable compteur, elle permet de compter combien de valeurs sont présentes dans le tableau

//boucle qui permet d'afficher les nombres présents dans le tableau 
for (let nombre of listeValeurs){
    console.log(nombre);                
    somme = somme + nombre ;                //on calcule la somme des nombres dans le tableau
    compteur = compteur +1 ;                //on compte le nombre de valeurs présentes dans le tableau
}

moyenne = somme / compteur ;                //on calcule la moyenne du tableau 

//Affichage de la somme
console.log("Somme des valeurs : " + somme );
//Affichage de la moyenne 
console.log("Moyenne des valeurs : "+ moyenne.toFixed(2) )          // on met 2 chiffres après la virgule