//Fichier "tab-patients-imc"
//
//
//Pottier Sarah le 04/05/2021


function Patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {
    this.nom = prmNom;                             //nom
    this.prenom = prmPrenom;                       //prenom
    this.age = prmAge;                             //age
    this.sexe = prmSexe;                           //sexe
    this.taille = prmTaille;                         //taille
    this.poids = prmPoids;                            //poids
    this.IMC = this.poids / ((this.taille / 100) * (this.taille / 100));                    //imc

}




//Création patients
let objPatient0 = new Patient('Dupond', 'Jean', 30, 'masculin', 180, 85);
let objPatient1 = new Patient('Martin', 'Eric', 42, 'masculin', 165, 90);
let objPatient2 = new Patient('Moulin', 'Isabelle', 46, 'féminin', 158, 74);
let objPatient3 = new Patient('Verwaerde', 'Paul', 55, 'masculin', 177, 66);
let objPatient4 = new Patient('Durand', 'Dominique', 60, 'féminin', 163, 54);
let objPatient5 = new Patient('Lejeune', 'Bernard', 63, 'masculin', 158, 78);
let objPatient6 = new Patient('Chevalier', 'Louise', 35, 'féminin', 170, 82);

//création du tableau
let tabPatients = [objPatient0, objPatient1, objPatient2, objPatient3, objPatient4, objPatient5, objPatient6];

//fonction qui affiche la liste des noms et des prénoms des patients
function afficher_ListePatients(prmTabPatients) {
    let affichage = "";                                 //affichage
    ///boucle qui parcourt tout le tableau
    for (let patient of prmTabPatients) {
        affichage = affichage + patient.nom + " " + patient.prenom + "\n";              // on met le nom et le prenom dans l'affichage

    }
    return affichage;
};

//Affichage dans la console du navigateur
console.log("Liste des patients : \n");
//appel de la fonction pour l'affichage de la lsite
console.log(afficher_ListePatients(tabPatients));


//Fonction qui permet de faire la liste des paetients par sexe, il y a 2 paramètres, le tableau avec les patients et le sexe
function afficher_ListePatient_Par_Sexe(prmTabPatients, prmSexe) {
    let affichage = "";                                 //affichage
    ///boucle qui parcourt tout le tableau
    for (let patient of prmTabPatients) {
        if (prmSexe == patient.sexe) {
            affichage = affichage + patient.nom + " " + patient.prenom + "\n";              // on met le nom et le prenom dans l'affichage
        }


    }
    return affichage;
};

//Affichage de la liste des patients de sexe féminin
console.log("Liste des patients de sexe féminin \n");
console.log(afficher_ListePatient_Par_Sexe(tabPatients, "féminin"));

//Affichage de la liste des patients de sexe masculin
//console.log("Liste des patients de sexe masculin \n") ;
//console.log(afficher_ListePatient_Par_Sexe(tabPatients, "masculin"));


//Fonction qui permet de faire la liste des paetients par corpulence, il y a 2 paramètres, le tableau avec les patients et la corpulence 
function afficher_ListePatients_Par_Corpulence(prmTabPatients, prmCorpulence) {
    console.log("Liste des patients en état de " + prmCorpulence);
    let message = " ";
    for (let patient of prmTabPatients) {

        let imc_corrige = patient.IMC;                    //valeur imc après modification avec le sexe

        //Si le patient est un homme
        if (patient.sexe == 'masculin') {
            imc_corrige = patient.IMC - 2;             //son imc est baissé est de 2 pour rajouter la marge
        }

        //On teste chaque valeur de l'imc pour lui donner une interprétation
        if (imc_corrige < 16.5) {
            interpretation = "Dénutrition";
        }
        else if (imc_corrige < 18.5) {
            interpretation = "Maigreur";
        }
        else if (imc_corrige < 25) {
            interpretation = "corpulence normale";
        }
        else if (imc_corrige < 30) {
            interpretation = "Surpoids";
        }
        else if (imc_corrige < 35) {
            interpretation = "Obésité modérée";
        }
        else if (imc_corrige < 40) {
            interpretation = "Obésité sévère ";
        }
        else {
            interpretation = "Obésité morbide";
        }
        /*
        //on verifie si c'est une femme ou un homme pour savoir quel pronom mettre (elle ou il)
        if (patient.sexe == 'féminin') {
            interpretation = "\nElle est en situation de : " + interpretation;

        } else {
            interpretation = "\nIl est en situation de : " + interpretation;
        }*/

        //quand l'interpretation sera la même que la corpulence choisit alors on affichera le nom du patient
        if (interpretation == prmCorpulence) {
            //affichage du patient
            message = message + patient.prenom + " " + patient.nom + " avec un IMC = " + patient.IMC.toFixed(2) + "\n";


        }

    }
    return message;     //retourne le message 
};

console.log(afficher_ListePatients_Par_Corpulence(tabPatients, 'Surpoids'));       // Appel de la fonction et affichage.


function afficher_DescriptionPatient(prmTabPatients, prmNom) {
    let message = "Ce nom n'existe pas";
    let interpretation = "";
    console.log("Description du patient " + prmNom);
    for (let patient of prmTabPatients) {
        if (prmNom == patient.nom) {            //on vérifie que le patient est dans la liste
            
            let taille_metre = Math.floor(patient.taille / 100);
            let taille_cm = patient.taille - (taille_metre * 100);
            //On vérifie si c'est un homme ou une femme pour changer les accords

            let imc_corrige = patient.IMC;                    //valeur imc après modification avec le sexe
            
            //Si le patient est un homme
            if (patient.sexe == 'masculin') {
                imc_corrige = patient.IMC - 2;             //son imc est baissé est de 2 pour rajouter la marge
            }

            //On teste chaque valeur de l'imc pour lui donner une interprétation
            if (imc_corrige < 16.5) {
                interpretation = "Dénutrition";
            }
            else if (imc_corrige < 18.5) {
                interpretation = "Maigreur";
            }
            else if (imc_corrige < 25) {
                interpretation = "corpulence normale";
            }
            else if (imc_corrige < 30) {
                interpretation = "Surpoids";
            }
            else if (imc_corrige < 35) {
                interpretation = "Obésité modérée";
            }
            else if (imc_corrige < 40) {
                interpretation = "Obésité sévère ";
            }
            else {
                interpretation = "Obésité morbide";
            }

            //on verifie si c'est une femme ou un homme pour savoir quel pronom mettre (elle ou il)
            if (patient.sexe == 'féminin') {
                message = "La patiente " + patient.prenom + " " + patient.nom + " est agée de " + patient.age + " ans. Elle mesure " + taille_metre + "m" + taille_cm + " et pèse " + patient.poids + " kg\n L'IMC de ce patient est de "+ patient.IMC.toFixed(2)+ "\n";
                message = message + "\nElle est en situation de : " + interpretation + "\n";

            } else {
                message = "Le patient " + patient.prenom + " " + patient.nom + " est agé de " + patient.age + " ans. Il mesure " + taille_metre + "m" + taille_cm + " et pèse " + patient.poids + " kg\n L'IMC de ce patient est de "+ patient.IMC.toFixed(2)+ "\n";
                message = message + "\nIl est en situation de : " + interpretation + "\n ";
            }




        }else{  //sinon le patient n'existe pas
            message;
        }


    }
    return message ;
};

console.log(afficher_DescriptionPatient(tabPatients, 'Dupond')); 
console.log(afficher_DescriptionPatient(tabPatients, 'Brassart')); 