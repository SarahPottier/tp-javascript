//Fichier "liste-articles.js"
//déclarer un tableau multidimensionnel, affichage de la liste avec toutes les données
//Calcul du  nombre d'articles et du montant total
//Affichage en mode console
//Pottier Sarah le 04/05/2021

//Création du tableau multidimensionnel
let listeArticles = [['Jus d\'orange 1L',1.35, 2],['Yaourt nature 4X', 1.6, 1],['Pain de mie 500g', 0.9, 1],['Barquette Jambon blanc 4xT', 2.75, 1],['Salade Laitue', 0.8,1],['Spaghettis 500g', 0.95, 2]];

let nbArticles = 0;             //nombre articles 
let somme = 0;                  //montant de la liste de course

//Affichage de la liste de course, du prix total en fonction du nombre d'articles
for (let articles of listeArticles){
    console.log(articles[0]+ "\n "+ articles[2] + " X "+ articles[1] + " EUR \t"+ articles[1]*articles[2] + " EUR" );
    somme = somme + (articles[1]*articles[2]);              //calcul du nombre d'articles
    nbArticles = nbArticles + articles[2];                  //calcul du montant de la liste de course
}


//Affichage du nombre d'articles achetés
console.log("Nombre d'articles achetés : " + nbArticles);

//Affichage du montant total des achats
console.log("MONTANT : " + somme.toFixed(2) + " EUR");