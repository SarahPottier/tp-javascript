# Activité 2 : Gestion d'une liste d'articles

## Explication
>* déclarer et initialiser un tableau multidimensionnel dans lequel chaque article est défini lui même par un tableau de trois éléments : une chaine de caractères correspondant à la désignation de l'article, un nombre correspondant au prix unitaire en euros et un nombre correspondant à la quantité achetée. Affichage de la liste des articles avec, pour chaque article, la quantité achetée et le prix correspondant. Puis cacluler et afficher le nombre total d'articles ainsi que le montant total des achats.


## Affichage du test du programme dans la console

  ![Image du test de la console](images/photo-console.png)