//Fichier calculer-imc.js
//Calculer et interpréter l'imc avec des fonctions
//Affichage de l'imc et de l'interprétation dans la console
//Pottier Sarah le 07/04/2021

function calculerIMC(prmTaille,prmPoids) {
    let valIMC;
    //Calcul de l'imc
    valIMC = (prmPoids / ((prmTaille/100)*(prmTaille/100))) ;

    return valIMC;
}


function interpreterIMC(prmIMC) {
    let interpretation = "";
    //On teste chaque valeur de l'imc pour lui donner une interprétation
    if (IMC < 16.5){
        interpretation = "Dénutrition" ;
    }
    else if (IMC < 18.5){
        interpretation = "Maigreur" ;
    }
    else if (IMC < 25){
        interpretation = "Corpulence normale" ;
    }
    else if (IMC < 30){
        interpretation = "Surpoids" ;
    }
    else if (IMC < 35){
        interpretation = "Obésité modérée" ;
    }
    else if (IMC < 40){
        interpretation = "Obésité sévère " ;
    }
    else {
        interpretation = "Obésité morbide" ;
    }
    return interpretation;
}

let taille = 169 ;
let poids = 59 ;
let IMC;
let interpretation ;

IMC = calculerIMC(taille, poids);
interpretation = interpreterIMC(IMC);

console.log ("L'IMC d'une persone qui mesure " + taille + "cm, et qui pèse " + poids + "kg est de : " + IMC.toFixed(1));
console.log("L'interprétation de son IMC est : "+ interpretation ) ;

