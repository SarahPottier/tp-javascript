# Activité Calculer IMC et interprétation avec des fonctions

## Explication
> Calcul de l'imc d'une personne et interprétation. On utilise deux fonctions pour faire ceci.
> Une qui calcule l'imc et une qui permet de faire l'interprétation.

## Affichage du test du programme dans la console

  ![Image du test de la console](images/test_console.png)