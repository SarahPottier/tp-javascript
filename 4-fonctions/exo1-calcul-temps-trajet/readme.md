# Exercice 1 Calculer le temps de parcours d'un trajet

## Explication
>Calcul du temps de parcours d'un trajet sachant la vitesse et la distance à parcourir. Une première fonction permet de mettre le temps de parcours en secondes puis une seconde fonction permet de mettre le temps de parcours en heures, minutes et secondes.

## Affichage du test du programme dans la console (v.1)

  ![Image du test de la console](images/test_consoleV1.png)



## Affichage du test du programme dans la console (v.2)

  ![Image du test de la console](images/test_cosnole_v2.png)