//Fichier calcul-temps-trajet.js
//Affichage du temps de parcours d'un trajet avec une distance et une vitesse donée
//Pottier Sarah le 08/04/2021


//Fonctions 
//Elle permet de calculer le temps de parcours en secondes
function calculerTempsParcoursSec(prmDistance, prmVitesse){
    let tempsParcours ;             //durée du parcours

    tempsParcours = (prmDistance/prmVitesse)*3600 ;         //calcul du temps en secondes

    return tempsParcours ;                                  //on retourne le résultat du calcul


}

//Elle permet de convertir la durée du parcours que l'on a calculé en heure minutes et secondes
function convertir_h_min_sec(prmTemps){
    let heures ;                        //heure 
    let minutes ;                       //minutes
    let secondes ;                      //secondes      
    let chaine ;                        //chaine de caractère permettant d'afficher les heures, minutes et secondes
    

    //Conversion 
    heures = prmTemps/3600;                         //on prend le temps calculé et on le met en heure 
    minutes = (prmTemps % 3600) / 60;               //ensuite on calcule les minutes 
    secondes = (prmTemps % 3600) % 60;              //puis on calcule les secondes
    //Affichage de la chaine de caractère 
    chaine = Math.floor(heures) + "H  " + Math.floor(minutes) + "mm  " + Math.floor(secondes) + "s" ; //on utilise ici Math.floor pour prendre la valeur entière

    return chaine ;                                 //retourne la chaine de caractère

}


//Déclaration des variables
let distance = 500;                                     //distance en km    
let vitesse = 90 ;                                      //vitesse en km/h
let temps = calculerTempsParcoursSec(distance, vitesse) ;       //temps en s, on appelle la fonction pour pouvoir faire le calcul 

//on appelle la fonction pour convertir les secondes dans le bon format
let tempsConvertit =  convertir_h_min_sec(temps);                                    //temps convertit en heure, minute et seconde


//Affichage dans la console 
console.log("Calcul du temps de parcours d'un trajet : ");
console.log("Vitesse moyenne (en km/h) : \t"+ vitesse);
console.log("Distance à parcourir (en km) : \t"+ distance);
console.log("A "+ vitesse + " km/h, une distance de "+ distance +" km est parcourue en "+ tempsConvertit);