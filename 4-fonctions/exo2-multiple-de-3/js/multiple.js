//Fichier multiple.js
//Affichage des multiples de 3 de 0 à un nombre choisi
//Pottier Sarah le 08/04/2021


//Fonctions 
//La fonction 
function rechercher_Mult3(prmNb){
    let chaine = 0;                         //chaine de caractère des multiples de 3
    let nb = 1 ;                            //nombre à tester 
    
    //tant que le nb à tester est inférieur au nombre choisi 
    while (nb <= prmNb){                   
        if (nb%3 == 0){                     //si le nombre à tester est un multiple de 3 
            chaine = chaine + "-" + nb ;    //on le rajoute dans la chaine de caractère
        }
        nb++;                               //on incrémente le nombre 
    }

    return chaine ;                         //on retourne la chaine 
}


//Déclaration des variables
let nombre = 20;                                //nombre choisi
let multiple = rechercher_Mult3(nombre);        //multiple du nombre choisi, on appelle la fonction qui trouvera tous les mutliples et les mettra dans une chaine de caractère

//Affichage dans la console 
console.log("Recherche des multiples de 3 :");
console.log("Valeur limite de la recherche : " + nombre );
console.log("Les multiples de 3 sont " + multiple);