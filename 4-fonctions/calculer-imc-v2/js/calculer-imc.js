//Fichier calculer-imc.js
//Calculer et interpréter l'imc avec des fonctions imbriquées 
//Affichage de l'imc et de l'interprétation dans la console
//Pottier Sarah le 27/04/2021


function decrire_corpulence(prmTaille, prmPoids) {

    function calculerIMC(prmTaille, prmPoids) {
        //Fonction qui permet de calculer l'IMC
        let valIMC;
        valIMC = (prmPoids / ((prmTaille/100)*(prmTaille/100))) ;       //Calcul de l'IMC

        return valIMC;              //retourne la valeur de l'IMC
    }

    let prmIMC = calculerIMC(prmTaille,prmPoids);       //je mets la valeur de l'IMC dans une variable prmIMC

    function interpreterIMC(prmIMC) {
        //Fonction qui permet de faire l'interpretation de l'IMC 
        let interpretation = "";
        //On teste chaque valeur de l'imc pour lui donner une interprétation
        if (prmIMC < 16.5) {
            interpretation = "Dénutrition";
        }
        else if (prmIMC < 18.5) {
            interpretation = "Maigreur";
        }
        else if (prmIMC < 25) {
            interpretation = "Corpulence normale";
        }
        else if (prmIMC < 30) {
            interpretation = "Surpoids";
        }
        else if (prmIMC < 35) {
            interpretation = "Obésité modérée";
        }
        else if (prmIMC < 40) {
            interpretation = "Obésité sévère ";
        }
        else {
            interpretation = "Obésité morbide";
        }
        return interpretation;
    }
    
    let interpretation = interpreterIMC(prmIMC);            //je mets l'interpretation dans une variable
    let message = "la valeur de l'IMC de cet individu est " + prmIMC.toFixed(1) + " et l'interprétation de l'imc est : " + interpretation;
    //variable message qui permet de mettre un message pour donner l'IMC et l'interpretation
    return message;     //retourne le message 

}
let decrireImc = decrire_corpulence (170,100);
console.log (decrireImc);
