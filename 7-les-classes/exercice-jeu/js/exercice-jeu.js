//Fichier "exercice-jeu.js"
// il s'agit de reprendre l'application vue dans le chapitre 
//sur les objets et qui permet de créer des objets représentant 
//les personnages d'un jeu. L'objectif sera d'en faire une nouvelle version 
//dans laquelle ces objets seront créés en utilisant les classes
//Pottier Sarah le 18/05/2021

class Personnage {
    constructor(prmNom, prmNiveau){ 
    //crée un personnes en lui donnant un nom et un niveau
    this.nom = prmNom ;
    this.niveau = prmNiveau ;
    }

    saluer (){
        //elle permet au personnage de se présenter 
        let presentation ;
        presentation = this.nom + ' vous salue !!' ;
        return presentation;
    }
}

//Création constructeur Guerrier() qui hérite du constructeur Personnage ()
class Guerrier extends Personnage {
    constructor(prmNom, prmNiveau, prmArme) {
    //appelle la fonction rattachée à l'objet donné
    super( prmNom, prmNiveau);
    this.arme = prmArme;
    }
    combattre (){
        //retourne un message qui décrit avec quel arme le personnage se bat 
        let description ;
        description = this.nom + ' est un guerrier qui se bat avec ' + this.arme;
        return description ;
    }
}



//Création constructeur Magicien() qui hérite du constructeur Personnage ()
class Magicien extends Personnage{
    constructor(prmNom, prmNiveau, prmPouvoir){
    super ( prmNom, prmNiveau);
    this.pouvoir = prmPouvoir;

    }
    posseder () {
        //retourne un message avec le nom du pouvoir que possède le personnage 
        let description;
        description = this.nom + ' est un magicien qui possède le pouvoir de ' + this.pouvoir ;
        return description ;
    }
}


//ajout d'une méthode au prototype du constructeur Magicien()
Magicien.prototype.posseder = function () {
    //retourne un message avec le nom du pouvoir que possède le personnage 
    let description;
    description = this.nom + ' est un magicien qui possède le pouvoir de ' + this.pouvoir ;
    return description ;
}


//Création du premier personnage de type Guerrier 
let objPersonnage1 = new Guerrier('Arthur', 3, 'épée') ;
//Affichage dans la console de la présentation du personnage 
console.log(objPersonnage1.saluer());
//Affichage dans la console de la description du personnage
console.log(objPersonnage1.combattre());

//Création du deuxième personnage de type Magicien
let objPersonnage2 = new Magicien('Merlin', 2, 'prédire les batailles') ;
//Affichage dans la console de la présentation du personnage 
console.log(objPersonnage2.saluer());
//Affichage dans la console de la description du personnage
console.log(objPersonnage2.posseder());

