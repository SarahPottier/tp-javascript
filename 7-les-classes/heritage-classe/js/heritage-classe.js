//Fichier "heritage-classe.js"
//Créer une nouvelle version de l'application qui permet de créer 
//des objets représentant des professeurs
// ou des élèves en utilisant l'héritage par classe.
//Pottier Sarah le 18/05/2021

class Personne {
    //Constructeur de la classe Personne
    constructor(prmNom, prmPrenom, prmAge, prmSexe) {
        this.nom = prmNom;
        this.prenom = prmPrenom;
        this.age = prmAge;
        this.sexe = prmSexe ;
    }
    //fonction qui décrit une personne
    decrire() {
        let description;
        description = this.prenom + " " + this.nom + " est agé de " + this.age + " ans"; 
        return description;
    }
}

//classe Professeur qui hérite de la classe Personne
class Professeur extends Personne {
    //Constructeur
    constructor(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {
        
        super(prmNom, prmPrenom, prmAge,prmSexe);  // appel du constructeur de la classe Personne
        this.matiere = prmMatiere;

    }

    //Fonction qui décrit avec plus d'informations sur un Professeur
    decrire_plus () {
        let description;
        let prefixe;
        //si la personne est un homme
        if (this.sexe == 'M') {
            prefixe = 'Mr';         //on mettra le prefixe 'Mr'
        } else {
            prefixe = 'Mme';        //Sinon on mettra 'Mme'
        }
        //variable qui va servir à l'affichage de la description
        description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
        return description;
    }
}

//classe Eleve qui hérite de la classe Personne
class Eleve extends Personne {
    //constructeur
    constructor(prmNom,prmPrenom,prmAge,prmSexe,prmClasse){
        super(prmNom,prmPrenom,prmAge,prmSexe);             // appel du constructeur de la classe Personne
        this.classe = prmClasse ;

    }
    //fonction qui décrit avec plus de détails un élève 
    decrire_plus () {
        let description;
        let pronom;
        //si la personne est un homme
        if (this.sexe == 'M') {
            pronom = 'un';              //on mettra le pronom 'un'
        } else {
            pronom = 'une';             //sinon on mettra 'une'
        }
        //variable qui va servri à l'affichage de la description à tout objet crée à partir du constructeur Eleve()
        description = this.prenom + " " + this.nom + " est " + pronom + " élève de " + this.classe;
        return description;  
    }
    
}


//Creation du professeur
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
//Creation de l'eleve
let objEleve = new Eleve('Dutillieul', 'Dorian', 18, 'M', 'SNIR1');

//Affichage
//Description du professeur
console.log(objProfesseur1.decrire());
//Description supplémentaire du professeur
console.log(objProfesseur1.decrire_plus());
//Description de l'eleve
console.log(objEleve.decrire());
//Description supplémentaire de l'eleve
console.log(objEleve.decrire_plus());

