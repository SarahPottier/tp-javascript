# **TP JavaScript**

***Bienvenue, je suis en BTS SNIR  et voici mes TP en JavaScript !***

>* Auteur : *Sarah POTTIER*
>* Date de publication : 06/04/2021

## **Sommaire**

1. *Introduction* 
    - [HelloWorld](1-introduction/HelloWorld_JS/index.html)

___

2. *Variables et opérateurs*
    - [Activité 1 : Calcul de Surface](2-variables-et-operateurs/calcul-surface/index.html)
    - [Exercice 1 : Calcul imc](2-variables-et-operateurs/calcul-imc/index.html)
    - [Exercice 2 : Conversion Celcius(°C)/Fahrenheit (°F) ](2-variables-et-operateurs/conversion-c-to-f/index.html)

___

3. *Structures de contrôle*
    - [Activité 1 : Interpretation IMC](3-structure-de-controle/interpretation-imc/index.html)
    - [Activité 2 : Suite Syracuse](3-structure-de-controle/suite-syracuse/index.html)
    - [Exercice 1 : Calcul de factorielle](3-structure-de-controle/exo1-calcul-factorielle/index.html)
    - [Exercice 2 : Conversion Euros en Dollars](3-structure-de-controle/exo2-conversion-euro-to-dollar/index.html)
    - [Exercice 3 : Nombres triples](3-structure-de-controle/exo3-nombres-triples/index.html)
    - [Exercice 4 : Suite de Fibonacci](3-structure-de-controle/exo4-suite-de-fibonacci/index.html)
    - [Exercice 5 : Table de multplication](3-structure-de-controle/exo5-table-de-multiplication/index.html)

___

4. *Fonctions* 
    - [Activité : Calcul de l'IMC et Interprétation avec des fonctions](4-fonctions/calculer-imc-v1/index.html)
    - [Activité : Calcul de l'IMC et Interprétation avec des fonctions imbriquées](4-fonctions/calculer-imc-v2/index.html)
    - [Exercice 1 : Calcul du temps de trajet](4-fonctions/exo1-calcul-temps-trajet/index.html)
    - [Exercice 2 : Recherche du nombre de multiples de 3](4-fonctions/exo2-multiple-de-3/index.html)
  
___

5. *Les objets*
    - [Activité : Création d'un objet littéral pour le calcul de l'IMC](5-les-objets/activite-imc/index.html)
    - [Activité : Création d'un objet littéral pour le calcul de l'IMC version 2](5-les-objets/activite-imc-v2/index.html)
    - [Activité : Codage d'un constructeur d'objet pour le calcul de l'IMC](5-les-objets/activite-imc-v3/index.html)
    - [Activité : Optimisation du codage du constructeur pour le calcul de l'IMC](5-les-objets/activite-imc-v4/index.html)
    - [Activité : Implémentation de l'héritage pour gérer les professeurs et les élèves](5-les-objets/heritage-proto/index.html)
    - [Exercice : Jeu sur les objets](5-les-objets/exercice/index.html)

___

6. *Les tableaux*
    - [Activité 1 : Calcul de la moyenne d'une série de valeurs](6-les-tableaux/calcul-moyenne/index.html)
    - [Activité 2 : Gestion d'une liste d'articles](6-les-tableaux/liste-articles/index.html)
    - [Activité 3 : Utilisation d'un tableau pour gérer une liste de patients au niveau du calcul de l'IMC](6-les-tableaux/tableau-patient-imc/index.html)
    - [Exercie : Calcul et affichage d'un histogramme ](6-les-tableaux/histogramme/index.html)


___

7. *Les classes*
    - [Activité 1 : Codage d'une classe pour l'application de calcul de l'IMC](7-les-classes/activite-imc/index.html)
    - [Activité 2 : Implémentation de l'héritage par classes pour gérer les professeurs et les élèves](7-les-classes/heritage-classe/index.html)
    - [Exercice sur le jeu des personnages](7-les-classes/exercice-jeu/index.html)


___

8. *Les chaînes de caractères*
    - [Acitivté :  décodage de trame GPS](8-les-chaines-de-caracteres/decodage-trame-gps/index.html)