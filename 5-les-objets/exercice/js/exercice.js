// Fichier exercice.js
//On souhaite développer une application qui permet
// de créer des objets représentant les personnages dans un jeu.
//Affichage en mode console
//Pottier Sarah le 29/04/2021


//Création d'une fonction presonnage avec 2 paramètres 
function Personnage(prmNom, prmNiveau){ 
    //crée un personnes en lui donnant un nom et un niveau
    this.nom = prmNom ;
    this.niveau = prmNiveau ;
}

//ajout au prototype du constructeur Personnage la méthode saluer ()
Personnage.prototype.saluer = function (){
    //elle permet au personnage de se présenter 
    let presentation ;
    presentation = this.nom + ' vous salue !!' ;
    return presentation;
}

//Création constructeur Guerrier() qui hérite du constructeur Personnage ()
function Guerrier(prmNom, prmNiveau, prmArme) {
    //appelle la fonction rattachée à l'objet donné
    Personnage.call(this, prmNom, prmNiveau);
    this.arme = prmArme;
}

Guerrier.prototype = Object.create(Personnage.prototype);//création objet a partir du prototype du constructeur Personnage et on assigne cet objet au prototype Guerrier().
Guerrier.prototype.constructor = Guerrier;              //Réinitialisation de la propriété constructeur du prototype Guerrier () avec Guerrier.

//ajout d'une méthode au prototype du constructeur Guerrier ()
Guerrier.prototype.combattre = function (){
    //retourne un message qui décrit avec quel arme le personnage se bat 
    let description ;
    description = this.nom + ' est un guerrier qui se bat avec ' + this.arme;
    return description ;
}

//Création constructeur Magicien() qui hérite du constructeur Personnage ()
function Magicien (prmNom, prmNiveau, prmPouvoir){
    Personnage.call (this, prmNom, prmNiveau);
    this.pouvoir = prmPouvoir;
}

Magicien.prototype = Object.create(Personnage.prototype); //création objet a partir du prototype du constructeur Personnage et on assigne cet objet au prototype Magicien().
Magicien.prototype.constructor = Magicien;                //Réinitialisation de la propriété constructeur du prototype Magicien () avec Magicien.

//ajout d'une méthode au prototype du constructeur Magicien()
Magicien.prototype.posseder = function () {
    //retourne un message avec le nom du pouvoir que possède le personnage 
    let description;
    description = this.nom + ' est un magicien qui possède le pouvoir de ' + this.pouvoir ;
    return description ;
}


//Création du premier personnage de type Guerrier 
let objPersonnage1 = new Guerrier('Arthur', 3, 'épée') ;
//Affichage dans la console de la présentation du personnage 
console.log(objPersonnage1.saluer());
//Affichage dans la console de la description du personnage
console.log(objPersonnage1.combattre());

//Création du deuxième personnage de type Magicien
let objPersonnage2 = new Magicien('Merlin', 2, 'prédire les batailles') ;
//Affichage dans la console de la présentation du personnage 
console.log(objPersonnage2.saluer());
//Affichage dans la console de la description du personnage
console.log(objPersonnage2.posseder());

