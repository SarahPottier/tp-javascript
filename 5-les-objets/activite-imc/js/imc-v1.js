//Fichier imc-v1.js
//Utilisation d'un objet objPatient 
//Affichage d'un message de description d'une personne
//Affichage de l'imc et de l'interprétation dans la console
//Pottier Sarah le 27/04/2021


let objPatient = {
    nom: 'Dupond',
    prenom: 'Jean',
    age: 30,
    sexe: 'masculin',
    taille: 180,
    poids: 85,

    decrire: function () {
        let description;
        let taille_metre = Math.floor(this.taille / 100);               
        let taille_cm = this.taille - (taille_metre * 100);
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + taille_metre +"m" + taille_cm + " et pèse " + this.poids + " kg";
        return description;
    },

    calculerIMC: function () {
        //fonction qui calcule l'imc
        let IMC;                //création variable imc
        IMC = (this.poids / ((this.taille / 100) * (this.taille / 100)));             //calcul de l'imc 
        return IMC;                     //retourne imc
    },


    interpreter_IMC: function (prmIMC) {
        //fonction qui permet l'interprétation de l'imc
        let interpretation = "";                //permet de mettre l'interpreation par rapport a l'imc
        let message_inter = "";                 //permet de mettre le message par rapport à l'interpretation de l'imc

        //On teste chaque valeur de l'imc pour lui donner une interprétation
        if (prmIMC < 16.5) {
            interpretation = "Dénutrition";
        }
        else if (prmIMC < 18.5) {
            interpretation = "Maigreur";
        }
        else if (prmIMC < 25) {
            interpretation = "Corpulence normale";
        }
        else if (prmIMC < 30) {
            interpretation = "Surpoids";
        }
        else if (prmIMC < 35) {
            interpretation = "Obésité modérée";
        }
        else if (prmIMC < 40) {
            interpretation = "Obésité sévère ";
        }
        else {
            interpretation = "Obésité morbide";
        }

        message_inter = "Il est en situation de : " + interpretation ;
        return message_inter;               //retourne la varible qui permet la description de l'interpretation
    },


};

console.log(objPatient.decrire());              //affichage de la description du patient

let IMC = objPatient.calculerIMC();         //on met l'imc dans une variable

//message d'affichage 
console.log("Son IMC est de : "+ IMC.toFixed(2));          
console.log(objPatient.interpreter_IMC(IMC));  