//Fichier imc-v3.js
//Codage d'un constructeur d'objet pour le calcul de l'IMC
//Affichage d'un message de description d'une personne
//Affichage de l'imc et de l'interprétation dans la console
//Pottier Sarah le 27/04/2021


function Patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {
    this.nom = prmNom ;                             //nom
    this.prenom = prmPrenom ;                       //prenom
    this.age = prmAge ;                             //age
    this.sexe = prmSexe ;                           //sexe
    this.taille =prmTaille;                         //taille
    this.poids = prmPoids ;                         //poids

    this.decrire = function () {
        let description = "";
        let sexe = this.sexe ;
        let taille_metre = Math.floor(this.taille / 100);
        let taille_cm = this.taille - (taille_metre * 100);
        //On vérifie si c'est un homme ou une femme pour changer les accords
        if (sexe == 'feminin'){
            description = "La patiente " + this.prenom + " " + this.nom + " est agée de " + this.age + " ans. Elle mesure " + taille_metre +"m" + taille_cm + " et pèse " + this.poids + " kg";

        }else {
            description = "Le patient " + this.prenom + " " + this.nom + " est agé de " + this.age + " ans. Il mesure " + taille_metre +"m" + taille_cm + " et pèse " + this.poids + " kg";

        }
        
        return description;
    },

    this.decrire_corpulence = function () {
        let poids = this.poids;
        let taille = this.taille;
        let sexe = this.sexe ;
        let imc;
        let interpretation = "" ;
        let definition_corpulence = "" ;

        function calculerIMC () {
            //fonction qui calcule l'imc
            
            imc = (poids / ((taille / 100) * (taille / 100)));             //calcul de l'imc 
            return imc;                 //retourne variable imc calculé
        };
    
    
        function interpreter_IMC () {
            //fonction qui permet l'interprétation de l'imc
            let imc_corrige;                    //valeur imc après modification avec le sexe

            //Si le patient est un homme
            if (sexe == 'masculin'){
                imc_corrige = imc - 2 ;             //son imc est baissé est de 2 
            }else {
                imc_corrige = imc ;                 //si c'est une femme on garde le même
            }
            //On teste chaque valeur de l'imc pour lui donner une interprétation
            if (imc_corrige < 16.5) {
                interpretation = "Dénutrition";
            }
            else if (imc_corrige < 18.5) {
                interpretation = "Maigreur";
            }
            else if (imc_corrige < 25) {
                interpretation = "Corpulence normale";
            }
            else if (imc_corrige < 30) {
                interpretation = "Surpoids";
            }
            else if (imc_corrige< 35) {
                interpretation = "Obésité modérée";
            }
            else if (imc_corrige < 40) {
                interpretation = "Obésité sévère ";
            }
            else {
                interpretation = "Obésité morbide";
            }

            //on verifie si c'est une femme ou un homme pour savoir quel pronom mettre (il ou elle)
            if(sexe == 'feminin'){
                interpretation = "\nElle est en situation de : " + interpretation;

            }else {
            interpretation = "\nIl est en situation de : " + interpretation;
            }
            return interpretation;
    };
        
        imc = calculerIMC();    
         
        definition_corpulence = "Son IMC est de : "+ imc.toFixed(2) + interpreter_IMC(imc) ;            //definition de la corpulence avec imc et interpreation
        return definition_corpulence ;          //retourne la variable qui definit la corpulence
        

    }
   


};

//Création des objets
let objPatient1 = new Patient('Dupont', 'Jean', 30, 'masculin', 180, 85);
let objPatient2 = new Patient('Moulin', 'Isabelle', 46, 'feminin', 158, 74);
let objPatient3 = new Patient('Martin', 'Eric', 42, 'masculin', 165, 90);

//Affichage de la descritpion des objets
console.log(objPatient1.decrire());              //affichage de la description du patient
console.log(objPatient1.decrire_corpulence());   //affichage de son imc et son interpretation
console.log(objPatient2.decrire());              //affichage de la description du patient
console.log(objPatient2.decrire_corpulence());   //affichage de son imc et son interpretation
console.log(objPatient3.decrire());              //affichage de la description du patient
console.log(objPatient3.decrire_corpulence());   //affichage de son imc et son interpretation