// Fichier heritage-proto.js
//implémentation de l'héritage pour gérer les professeurs et les élèves
//Affichage en mode console
//Pottier Sarah le 29/04/2021


function Personne(prmNom, prmPrenom, prmAge, prmSexe) {
    this.nom = prmNom;                      //nom
    this.prenom = prmPrenom;                //prenom    
    this.age = prmAge;                      //age
    this.sexe = prmSexe;                    //sexe
}

//fonction qui permet la description de la personne
Personne.prototype.decrire = function () {
    let description;
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans";
    return description;
}

//Création du constructeur Professeur 
function Professeur(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {
    Personne.call(this, prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}

//Création du constructeur Eleve
function Eleve(prmNom, prmPrenom, prmAge, prmSexe, prmClasse) {
    Personne.call(this, prmNom, prmPrenom, prmAge, prmSexe);
    this.classe = prmClasse;
}

Eleve.prototype = Object.create(Personne.prototype); //création objet a partir du prototype du constructeur personne et on assigne cet objet au prototype Eleve().
Eleve.prototype.constructor = Eleve;    //Réinitialisation de la propriété constructeur du prototype Eleve () avec Eleve.

Professeur.prototype = Object.create(Personne.prototype);//création objet a partir du prototype du constructeur personne et on assigne cet objet au prototype Professeur().
Professeur.prototype.constructor = Professeur;  //Réinitialisation de la propriété constructeur du prototype Professeur() avec Professeur.

//Méthode qui permet d'afficher une descritpion à tout objet crée à partir du constructeur Professeur()
Professeur.prototype.decrire_plus = function () {
    let description;
    let prefixe;
    //si la personne est un homme
    if (this.sexe == 'M') {
        prefixe = 'Mr';         //on mettra le prefixe 'Mr'
    } else {
        prefixe = 'Mme';        //Sinon on mettra 'Mme'
    }
    //variable qui va servir à l'affichage de la description
    description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
    return description;
}

//Méthode qui permet d'afficher une descritpion à tout objet crée à partir du constructeur Eleve()
Eleve.prototype.decrire_plus = function () {
    let description;
    let pronom;
    //si la personne est un homme
    if (this.sexe == 'M') {
        pronom = 'un';              //on mettra le pronom 'un'
    } else {
        pronom = 'une';             //sinon on mettra 'une'
    }
    //variable qui va servri à l'affichage de la description à tout objet crée à partir du constructeur Eleve()
    description = this.prenom + " " + this.nom + " est " + pronom + " élève de " + this.classe;
    return description;  
}

//création de l'objProfesseur1
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
//affichage dans la console
console.log(objProfesseur1.decrire());
console.log(objProfesseur1.decrire_plus());

//création de l'objEleve1
let objEleve1 = new Eleve('Dutillieul', 'Dorian', 18, 'M', 'SNIR1');
//Affichage dans la console
console.log(objEleve1.decrire());
console.log(objEleve1.decrire_plus());


