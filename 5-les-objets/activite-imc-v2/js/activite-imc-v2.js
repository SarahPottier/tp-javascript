//Fichier activite-imc-v2.js
//Utilisation d'un objet objPatient mais avec des fonctions imbriquées 
//Affichage d'un message de description d'une personne
//Affichage de l'imc et de l'interprétation dans la console
//Pottier Sarah le 27/04/2021


let objPatient = {
    nom: 'Dupond',
    prenom: 'Jean',
    age: 30,
    sexe: 'masculin',
    taille: 180,
    poids: 85,

    decrire: function () {
        let description;
        //pour l'affichage de la forme "1m70"
        let taille_metre = Math.floor(this.taille / 100);               //pour avoir les mètres de la taille
        let taille_cm = this.taille - (taille_metre * 100);             //pour avoir les centimètres

        //description du patient avec ses données 
        description = "Le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + taille_metre +"m" + taille_cm + " et pèse " + this.poids + " kg";
        return description;         //retourne la variable description
    },

    decrire_corpulence : function () {
        let poids = this.poids;                     //poids
        let taille = this.taille;                   //taille    
        let imc;                                    //imc
        let interpretation = "" ;                   //interpretation de l'imc
        let definition_corpulence = "" ;            //permet la description globale avec affichage de l'imc et l'interpretaion 

        function calculerIMC () {
            //fonction qui calcule l'imc
            
            imc = (poids / ((taille / 100) * (taille / 100)));             //calcul de l'imc 
            return imc;
        };
    
    
        function interpreter_IMC () {
            //fonction qui permet l'interprétation de l'imc
            
            //On teste chaque valeur de l'imc pour lui donner une interprétation
            if (imc < 16.5) {
                interpretation = "Dénutrition";
            }
            else if (imc < 18.5) {
                interpretation = "Maigreur";
            }
            else if (imc < 25) {
                interpretation = "Corpulence normale";
            }
            else if (imc < 30) {
                interpretation = "Surpoids";
            }
            else if (imc < 35) {
                interpretation = "Obésité modérée";
            }
            else if (imc < 40) {
                interpretation = "Obésité sévère ";
            }
            else {
                interpretation = "Obésité morbide";
            }
            interpretation = "  et il est en situation de : " + interpretation
            return interpretation;                  //retourne la varible qui permet de donner la description de l'interpretation
    };
        
        imc = calculerIMC();          
        definition_corpulence = "Son IMC est de : "+ imc.toFixed(2) + interpreter_IMC(imc) ;            //definition de la corpulence avec imc et interpreation
        return definition_corpulence ;          //retourne la variable qui definit la corpulence
        

    }
   


};

//Affichage 
console.log(objPatient.decrire());              //affichage de la description du patient
console.log(objPatient.decrire_corpulence());   //affichage de son imc et son interpretation