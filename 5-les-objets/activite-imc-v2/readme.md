# Activité : Création d'un objet littéral pour le calcul de l'IMC version 2

## Explication
> Création d'une nouvelle version de l'objet qui utilise une méthode definir_corpulence() pour retourner en une seule fois le message contenant la valeur de l'IMC et l'état de corpulence correspondant. Les méthodes calculer_IMC() et interpreter_IMC() seront utilisées comme des fonctions internes.


## Affichage du test du programme dans la console

  ![Image du test de la console](images/photo-console.png)